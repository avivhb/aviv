import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.css']
})
export class CityFormComponent implements OnInit {

  cities:Object[] = [{id:1,name:'jerusalem'},{id:2,name:'tel-aviv'},{id:3,name:'london'},{id:4,name:'yokne-am'}];
  Selectedcity:string;

  onSubmit(){
    this.router.navigate(['/temperatures',this.Selectedcity]);
  }

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

}

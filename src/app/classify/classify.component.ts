import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  favoriteSeason: string;
  networks: string[] = ['BBC', 'CNN', 'Mako'];

  favoriteNetwork:string;

  text:string;
  categoryResult;
  categoryImage;

  sendText(){
    this.classifyService.classify(this.text).subscribe(
      res => {
        console.log(res);
        /*
        this.categoryResult = this.classifyService.categories[res];
        this.categoryImage.ImageService.images[res];
        */
      }
    )
  }

  constructor(private route:ActivatedRoute, private classifyService:ClassifyService, 
    private imageService:ImageService) { 
    this.favoriteNetwork = this.route.snapshot.params.network;
  }

  ngOnInit(): void {
  }

}

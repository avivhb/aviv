import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private URL = "https://atystxdwi6.execute-api.us-east-1.amazonaws.com/test";
  public categories:object = {0:'Bussiness',1:'Entertainment',2:'Politics',3:'sport',4:'Tech'};

  classify(text:string){
    let json = {'articles':[
      {'text':text}
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.URL,body).pipe(
      map(res => {
        console.log(res);
        /*
        let final:string = res.body;
        console.log(final);
        return final;
        */
      })
    )
  }

  constructor(private http:HttpClient) { }
}

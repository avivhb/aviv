import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-aviv.appspot.com/o/'
  public images:string[] = [];

  constructor( ) {
    this.images[0] = this.path + '1.JPG' + '?alt=media&token=0312e46e-a55a-4869-9bf1-d3da5cf3cae9';
    this.images[1] = this.path + '2.JPG' + '?alt=media&token=8c71f682-f600-44e2-83cd-6a7ed18cd75f';
    this.images[2] = this.path + '3.JPG' + '?alt=media&token=46c7129c-e604-49ad-b634-b2e85a564a8e';
   }
}

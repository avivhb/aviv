import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getbooks(userId,startAfter){
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref => ref.orderBy('title', 'asc').limit(4).startAfter(startAfter));
    return this.bookCollection.snapshotChanges()
  }

   deleteBook(userId:string,bookId:string){
    this.db.doc(`users/${userId}/books/${bookId}`).delete();
   }

   updateBook(userId:string,bookId:string,title:string,author:string){
     this.db.doc(`users/${userId}/books/${bookId}`).update(
       {
         title:title,
         author:author
       }
     )
   }

   addBook(userId:string,title:string,author:string){
     const book = {title:title, author:author};
     this.userCollection.doc(userId).collection('books').add(book);
   }

  constructor(private db:AngularFirestore) { }
}

import { Post } from './interfaces/post';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http:HttpClient) { }

  searchPosts():Observable<Post>{
    return this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/`);
  }

}

import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  city:string;

  temperature:number;
  image:string;
  country:string;
  weatherData$:Observable<any>;
  hasError:boolean = false;
  errorNessage:string;

  constructor(private route:ActivatedRoute, private weatherService:WeatherService) {
    this.city = this.route.snapshot.params.city;
   }
  
  ngOnInit(): void {

    this.weatherData$ = this.weatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(data => {
      this.temperature = data.temperature;
      this.image = data.image;
      this.country = data.country;
    },
    error => {
      this.hasError = true;
      this.errorNessage = error.message;
    })

  }

}

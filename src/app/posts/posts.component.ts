import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../interfaces/post';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$:Observable<Post>;

  constructor(private PostsService:PostsService) { }

  ngOnInit(): void {
    this.posts$ = this.PostsService.searchPosts();
  }

}

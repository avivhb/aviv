import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../interfaces/book';

@Component({
  selector: 'book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  @Input() title:string;
  @Input() author:string;
  @Input() bookId:string;

  @Input() formType:string;

  @Output() update = new EventEmitter<Book>();
  @Output() closeEdit = new EventEmitter<null>(); //close edit


  updateParent(){
    let book:Book = {id:this.bookId, title:this.title, author:this.author};
    this.update.emit(book);
    if(this.formType == "Add Book"){
      this.title = null;
      this.author = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit();
  }


  constructor() { }

  ngOnInit(): void {
  }

}

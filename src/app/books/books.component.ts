import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Book } from '../interfaces/book';
import { title } from 'process';


@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  books$;
  userId:string;
  
  hiddenEdit = [true];
  hiddenBook:boolean = true;

  books:Book[]
  lastDocumentArrived;  

  constructor(private booksService:BooksService, public authService:AuthService) {  }

  deleteBook(bookId:string){
    this.booksService.deleteBook(this.userId,bookId);
  }

  update(book:Book){
    this.booksService.updateBook(this.userId,book.id,book.title,book.author);
  }

  add(book:Book){
    this.booksService.addBook(this.userId,book.title,book.author);
  }

  nextPage(){
        this.books$ = this.booksService.getbooks(this.userId,this.lastDocumentArrived); 
        this.books$.subscribe(
          docs =>{
            this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
            this.books = [];
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id; 
              this.books.push(book); 
            }
          }
        )     
      }


  ngOnInit(): void {

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.booksService.getbooks(this.userId,null);

        this.books$.subscribe(
          docs =>{
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.books = [];
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id; 
              this.books.push(book); 
          }
      }
    )
  }
    )

}
}
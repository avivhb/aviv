// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCN_3SGgUmqjP1qj-TPYRCADAeFpeHJj0k",
    authDomain: "aviv-88553.firebaseapp.com",
    projectId: "aviv-88553",
    storageBucket: "aviv-88553.appspot.com",
    messagingSenderId: "251247745638",
    appId: "1:251247745638:web:1b32e79e8fda6893256ba7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
